* Edit file `/usr/share/X11/xkb/rules/evdev.xml`

* Change `en` & `fa` in `<shortDescription>` tags with country flags emojies which can be founded in [getemoji.com](https://getemoji.com)

* Restart Gnome shell (`Alt+F2` + `R` + `Enter`)
