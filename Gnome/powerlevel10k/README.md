**In the name of Allah**

# Customizing Manjaro ZSH line

* Put the `p10k.zsh` file in home directory.

* Colores & Some other configs could be found in [This link](https://github.com/Powerlevel9k/powerlevel9k/wiki/Stylizing-Your-Prompt).

* Distro icons code could be found [here](https://github.com/Powerlevel9k/powerlevel9k/issues/751#issuecomment-366397738).

* First you may have a config using `$ p10k configure` command.
