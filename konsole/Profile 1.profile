[Appearance]
AntiAliasFonts=true
BoldIntense=true
ColorScheme=OneDark
EmojiFont=Noto Color Emoji,13,-1,5,50,0,0,0,0,0
Font=Fira Code Medium,13,-1,5,57,0,0,0,0,0,Regular
IgnoreWcWidth=false
UseFontLineChararacters=false
WordModeAttr=false

[General]
Icon=goterminal
Name=Profile 1
Parent=FALLBACK/
SemanticInputClick=false
SemanticUpDown=false
TerminalMargin=10

[Terminal Features]
BlinkingCursorEnabled=true
