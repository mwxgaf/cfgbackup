vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

vim.opt.backspace = '2'
vim.opt.showcmd = true
vim.opt.laststatus = 2
vim.opt.autowrite = true
-- vim.opt.cursorline = true
vim.opt.autoread = true

-- use spaces for tabs and whatnot
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.shiftround = true
vim.opt.expandtab = true
vim.opt.number = true
vim.opt.termbidi = true

vim.keymap.set('n', '<c-s>',':Startify<CR>',{})
vim.keymap.set('n', '<Space><Space>',':nohlsearch<CR>',{})
vim.keymap.set('n', '<Space>w', ':w<cr>',{})
vim.keymap.set('n', '<Space>q', ':q<cr>',{})
vim.cmd [[set noshowmode]]
