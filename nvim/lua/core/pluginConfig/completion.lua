local cmp = require("cmp")

require('luasnip.loaders.from_vscode').lazy_load()

cmp.setup({
  mapping = cmp.mapping.preset.insert({
    ['<C-PageDown>'] = cmp.mapping.scroll_docs(4),
    ['<C-PageUp>'] = cmp.mapping.scroll_docs(-4),
    ['<C-Space>'] = cmp.mapping.complete(),
    -- ['<leader>e'] = cmp.mapping.abort(),
    ['<Tab>'] = cmp.mapping.confirm({ select = true }),
  }),
  snippet = {
    expand = function(args)
      require('luasnip').lsp_expand(args.body)
    end
  },
  sources = cmp.config.sources({
    {name = 'nvim_lsp'},
    {name = 'luasnip' }
  }, {
    {name = 'buffer'},
    {
      name = 'emmet_vim',
      options = {
        filetypes = {'html','xml','vue','typescriptreact','javascriptreact','css','sass','scss','less','heex','tsx','jsx'}
      }
    },
  }),
})
