require("mason").setup()
require("mason-lspconfig").setup(
{
   ensure_installed = { "lua_ls" }
})

local on_attach = function(_, _)
  vim.keymap.set('n', '<leader>r', vim.lsp.buf.rename, {})
  vim.keymap.set('n', '<leader>c', vim.lsp.buf.code_action, {})
  vim.keymap.set('n', '<f12>', vim.lsp.buf.definition, {})
  vim.keymap.set('n', '<s-f12>', vim.lsp.buf.implementaion, {})
  vim.keymap.set('n', 'H', vim.lsp.buf.hover, {})
  vim.keymap.set('n', '<leader>f', require('telescope.builtin').lsp_references, {})
end

local capabilities = require('cmp_nvim_lsp').default_capabilities()

require('lspconfig').lua_ls.setup {
  on_attach = on_attach,
  capabilities = capabilities,
}
require('lspconfig').intelephense.setup {
  on_attach = on_attach,
  capabilities = capabilities,
}
require('lspconfig').bashls.setup {
  on_attach = on_attach,
  capabilities = capabilities,
}
require('lspconfig').clangd.setup {
  on_attach = on_attach,
  capabilities = capabilities,
}
require('lspconfig').cssls.setup {
  on_attach = on_attach,
  capabilities = capabilities,
}
require('lspconfig').eslint.setup {
  on_attach = on_attach,
  capabilities = capabilities,
}
require('lspconfig').html.setup {
  on_attach = on_attach,
  capabilities = capabilities,
}
require('lspconfig').jsonls.setup {
  on_attach = on_attach,
  capabilities = capabilities,
}
require('lspconfig').tsserver.setup {
  on_attach = on_attach,
  capabilities = capabilities,
}
require('lspconfig').marksman.setup {
  on_attach = on_attach,
  capabilities = capabilities,
}
require('lspconfig').jedi_language_server.setup {
  on_attach = on_attach,
  capabilities = capabilities,
}
require('lspconfig').sqlls.setup {
  on_attach = on_attach,
  capabilities = capabilities,
}
require('lspconfig').volar.setup {
  on_attach = on_attach,
  capabilities = capabilities,
}
