vim.notify = require("notify")
require("notify").setup{
  stages = 'fade_in_slide_out',
  timeout = 3000,
}
