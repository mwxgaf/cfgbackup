vim.g.startify_bookmarks = {
  {d = './'},
  {w = '~/w'},
  {t = '~/t'},
  {z = '~/.zshrc'},
  {v = '~/.config/nvim'},
  {b = '~/.config/bspwm/bspwmrc'},
  {s = '~/.config/bspwm/sxhkdrc'},
  {x = '~/.tmux.conf'},
}
vim.g.startify_lists = {{header = {'   Bookmarks 🔰'}, type = 'bookmarks'}}
vim.g.startify_fortune_use_unicode = 1
vim.g.startify_custom_header = {
	'   In the name of Allah',
	'   __   __ ___  __  __ ',
	'   \\ \\ / /|_ _||  \\/  |',
	'    \\ V /  | | | |\\/| |',
	'     \\_/  |___||_|  |_|',
}
vim.g.startify_custom_footer = {
	'   Amirreza Aliakbari 👳',
	'   Saint Hacker 🙃',
	'   ✉️ @mwxgaf 🔗 mwxgaf.ir 🔑 6CB6 60EF B779 859B',
}
