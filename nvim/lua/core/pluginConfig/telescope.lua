local builtin = require('telescope.builtin')

vim.keymap.set('n', '<c-o>', builtin.find_files, {})
vim.keymap.set('n', '<Space>o', builtin.oldfiles, {})
vim.keymap.set('n', '<c-f>', builtin.live_grep, {})
vim.keymap.set('n', '<Space>h', builtin.help_tags, {})
