require'nvim-treesitter.configs'.setup {
  ensure_installed = {"python","lua","vim","dart","javascript","c","rust","php","sql","html","css","markdown","typescript",},
  sync_install = false,
  auto_install = true,
  hightlight = {
    enable = true,
  },
}
