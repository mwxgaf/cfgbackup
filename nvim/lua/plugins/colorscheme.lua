return {
  {
    "navarasu/onedark.nvim",
    lazy = true,
    opts = {
      style = "darker",
      transparent = true,
    },
  },

  {
    "LazyVim/LazyVim",
    opts = {
      colorscheme = "onedark",
    },
  },
  {
    "rcarriga/nvim-notify",
    opts = {
      background_colour = "#1e2128",
    },
  },
}
