#!/bin/sh

cd "$HOME/Projects/Pages/cv"
cp -rf *.shtml sitemap.xml index.html error.css style.min.css script.min.js assets donate fa skills ../amirthefree.ir
cd ../amirthefree.ir
rm skills/skills.js
git add -A
git commit -m"$(date)"
git push origin master
