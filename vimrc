set number
syntax on
set mouse=a
set encoding=UTF-8
set tabstop=4
set shiftwidth=4
set background=dark
set laststatus=2
set noshowmode
set termbidi
call plug#begin()
  Plug 'joshdick/onedark.vim'
  Plug 'preservim/nerdtree'
  Plug 'ryanoasis/vim-devicons'
  Plug 'jmcantrell/vim-virtualenv'
  Plug 'itchyny/lightline.vim'
  Plug 'valloric/youcompleteme'
  Plug 'scrooloose/syntastic'
  Plug 'airblade/vim-gitgutter'
  Plug 'mattn/emmet-vim'
  Plug 'jiangmiao/auto-pairs'
  Plug 'alvan/vim-closetag'
  Plug 'mhinz/vim-startify'
  Plug 'ervandew/supertab'
  Plug 'jistr/vim-nerdtree-tabs'
call plug#end()
let g:lightline = {
  \ 'colorscheme': 'onedark',
  \ }
nnoremap <C-t> :NERDTreeToggle<CR>
nnoremap <C-n> :Startify<CR>
nnoremap <C-S-Right> :tabn<CR>
nnoremap <C-S-Left> :tabp<CR>
nnoremap <C-S-Up> :tabnew<CR>
let g:ycm_autoclose_preview_window_after_completion = 1
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_warning_symbol = '⚠️'
let g:syntastic_error_symbol = '❌'
let g:startify_bookmarks = [
	\ {'w': '~/w'},
	\ {'t': '~/temp'},
	\ {'z': '~/.zshrc'},
	\ {'v': '~/.vimrc'},
	\ ]
let g:startify_lists = [
	\ {'header': ['   Bookmarks 🔰'],'type':'bookmarks'},
	\ ]
let g:startify_fortune_use_unicode = 1
let g:startify_custom_header = [
	\ '   In the name of Allah',
	\ '   __   __ ___  __  __ ',
	\ '   \ \ / /|_ _||  \/  |',
	\ '    \ V /  | | | |\/| |',
	\ '     \_/  |___||_|  |_|',
	\ ]
let g:startify_custom_footer = [
	\ '   Amirreza Aliakbari 👳',
	\ '   Saint Hacker 🙃',
	\ '   Revolutionary Shia & Geek 🐧',
	\ '   ✉️ @mwxgaf 🔗 mwxgaf.ir 🔑 6CB6 60EF B779 859B',
	\ ]
let g:user_emmet_settings = {
	\ 'indentation' : '   ',
	\ }
let g:user_emmet_expandabbr_key = '<C-e>'
colorscheme onedark
